from aenea import *
from breathe import Breathe, CommandContext
from voice2.utils import utilities
from voice2.utils.elements import Boolean, Choice
from voice2.utils.execution import Alternating

BINDINGS = utilities.load_toml_relative("config/python.toml")
LIB = utilities.load_toml_relative("config/python_libs.toml")

libs={}
map={
        "for each"              : Text("for  in :") + Key("left:5"),
        "for loop"              : Text("for i in range():") + Key("left:2"),
        "from import"           : Text("from  import ") + Key("home, right:5"),
        "function"              : Text("def ():") + Key("left:3"),
        "(if | iffy)"        : Text("if :") + Key("left"),
        "if not"             : Text("if not :") + Key("left"),
        # "import"                : Text("import "),
        "lambda"                : Text("lambda :") + Key("left"),
        "while loop"            : Text("while :") + Key("left"),
        "shell iffae"           : Text("elif :") + Key("left"),
        "shells"                : Text("else:"),
        "return"                : Text("return "),
        "say <vocabs>"          : Text("%(vocabs)s"),
    }

express=[Choice("vocabs", BINDINGS["common"]),]
for lib, data in LIB.iteritems():
    pronunciation = data.pop("pronunciation")
    name = data.pop("name") if "name" in data else lib
    importname=data.pop("import_as") if "import_as" in data else name
    libs[pronunciation] = name + " as " + importname

    #. e.g. "numb pie <numpy_lib>": Alternating("numpy_lib")
    map["%s"%pronunciation]=Text("%s."%importname)
    map["%s <%s_lib>" % (pronunciation, lib)] = Alternating("%s_lib" % lib)
    express.append(Choice("%s_lib" % lib, data))

map["import <lib>"] = Text("import %(lib)s")
map["from <lib> import"] = Text("from %(lib)s import ")
express.append(Choice("lib", libs))
# print(map)

Breathe.add_commands(
    # Commands will be active either when we are editing a python file
    # or after we say "enable python". pass None for the commands to be global.
    context = ProxyCustomAppContext(title=".py") | CommandContext("python"),
    mapping = map,
    extras = express
)