from aenea import *
from breathe import Breathe, CommandContext

Breathe.add_commands(
    ProxyCustomAppContext(title="fire"),
    {
     'new (window | win)': Key("c-n"),
    'new (tab | sub)': Key("c-t"),
    'close (tab | sub)': Key("c-w"),
    # 'search <text>': Key("c-k") + Text("%(text)s") + Key('enter'),
    'search clip': Key("c-k, c-v") + Key('enter'),
    'address bar': Key("a-d"),
    'search bar': Key("c-k"),
    'back [<n>]': Key("a-left:%(n)d"),
    'forward [<n>]': Key("c-right:%(n)d"),
    'next tab [<n>]': Key("c-pgdown:%(n)d"),
    'previous tab [<n>]': Key("c-pgup:%(n)d"),
    'normal text size': Key("c-0"),
    'bigger text size': Key("c-plus"),
    'smaller text size': Key("c-minus"),
    'show': Key("h"),
    'close (tabs|subs) <n>': Key("c-w/20:%(n)d"),
    'show bookmarks': Key("c-b"),
    },
    [

    ],

)