from aenea import *
from breathe import Breathe, CommandContext
from voice2.utils import utilities
from voice2.utils.actions import Read
from voice2.utils.elements import Boolean, Choice
from voice2.utils.execution import Alternating
from dragonfly import Modifier, ShortIntegerRef

BINDINGS = utilities.load_toml_relative("config/lyx.toml")


def greek(big, greek_letter):
    if big:
        greek_letter = greek_letter.title()
    Text("\\" + greek_letter + " ").execute()


def matrix(rows, cols):
    Text("\\" + BINDINGS["matrix_style"] + " ").execute()
    Pause("3").execute()
    Key("a-m, w, i, " * (rows - 1) + "a-m, c, i, " * (cols - 1)).execute()


Breathe.add_commands(
    context=ProxyCustomAppContext(executable="lyx") | CommandContext("looks"),
    mapping={
        "<control>":
            Key("%(control)s"),
        "<control_repeat> [<n>]":
            Key("%(control_repeat)s") * Repeat(extra="n"),
        BINDINGS["symbol1_prefix"] + " <symbol1>":
            Text("\\%(symbol1)s "),

        BINDINGS["symbol2_prefix"] + " <symbol2>":
            Text("\\%(symbol2)s "),

        BINDINGS["accent_prefix"] + " <accent>":
            Key("a-m, %(accent)s"),

        " <text_modes>":
            Text("\\%(text_modes)s "),

        BINDINGS["greek_prefix"] + " [<big>] <greek_letter>":
            Function(greek),

        "<misc_lyx_keys>":
            Key("%(misc_lyx_keys)s"),

        "<command>":
            Alternating("command"),

        "insert matrix ":Text("\\" + BINDINGS["matrix_style"] + " "),

        "<numbers> <denominator>":
            Key("a-m, f") + Text("%(numbers)s") + Key("down") + Text("%(denominator)s") + Key("right"),

    },

    extras=[
        Choice("control", BINDINGS["control"]),
        Choice("control_repeat", BINDINGS["control_repeat"]),
        IntegerRef("rows", 1, BINDINGS["max_matrix_size"]),
        IntegerRef("cols", 1, BINDINGS["max_matrix_size"]),
        IntegerRef("numbers", 0, 1000),
        Boolean("big"),
        Choice("greek_letter", BINDINGS["greek_letters"]),
        Choice("symbol1", BINDINGS["tex_symbols1"]),
        Choice("symbol2", BINDINGS["tex_symbols2"]),
        Choice("accent", BINDINGS["accents"]),
        Choice("text_modes", BINDINGS["text_modes"]),
        Choice("misc_lyx_keys", BINDINGS["misc_lyx_keys"]),
        Choice("command", BINDINGS["misc_lyx_commands"]),
        Choice("denominator", BINDINGS["denominators"]),
    ]
)
