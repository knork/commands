from aenea import *
from breathe import Breathe, CommandContext

Breathe.add_commands(
    ProxyCustomAppContext(executable="harm")|
ProxyCustomAppContext(executable="clion")|
    ProxyCustomAppContext(executable="virtual")| ProxyCustomAppContext(title=".py"),
    {
        "run app": Key("s-f10"),
        "apply (app|changes)": Key("a-end"),
        "build app": Key("c-f9"),
        "debug app": Key("s-f9"),
        "re-run app": Key("c-f5"),
        "run test": Key("cs-f10"),
        "force stop": Key("c-f2")+Key("cs-f4"),

        # Code navigation.
        "go to (class|test)": Key("cs-t"),
        "go to class  <text>": Key("c-n") + Pause("30") + Text("%(text)s") + Pause("30"),
        "go to file ": Key("cs-n") + Pause("30"),
        "go to file <text>": Key("cs-n") + Pause("30") + Text("%(text)s"),
        "go to symbol <text>": Key("cas-n") + Pause("30") + Text("%(text)s") + Pause("30"),
        "go to declaration": Key("c-b"),
        "go to implementation": Key("ca-b"),
        "go to super": Key("c-u"),
        "go to favourites": Key("a-2"),
        "go to version control": Key("a-9"),
        "go to terminal": Key("a-f12"),

        "back": Key("ca-left"),
        "skip": Key("cs-m"),
        "select more": Key("c-w"),
        "select less": Key("cs-w"),

        # Project settings.
        "project": Key("a-1"),
        "structure": Key("a-7"),
        "module settings": Key("f4"),
        "[project] settings": Key("ca-s"),
        "action": Key("cs-a"),
        "synchronize files": Key("ca-y"),

        # Terminal.
        "run terminal": Key("a-f12"),

        # Search.thisExposure context
        # "find next [<n>]": Key("c-f3")*Repeat("n"),
        # "find previous [<n>]": Key("sc-f3")*Repeat("n"),
        "find in path": Key("cs-f"),
        "find usages": Key("a-f7"),
        "replace": Key("c-r"),

        # Edit.
        "save this next[file|all]": Key("c-s"),
        "comment [<n>]": Key("c-npdiv:%(n)d"),
        "format code": Key("ca-l"),

        # Code.
        "(show intentions|alt slab)": Key("a-enter"),
        "show": Key("c-space"),
        "show documentation": Key("c-q"),
        "show members": Key("c-f12"),
        "go to line": Key("c-g"),
        "debug line": Key("c-f8"),

        "line <line>": Key("c-g/25") + Text("%(line)d") + Pause("9") + Key("enter"),
        "implement method": Key("c-i"),
        "override method": Key("c-o"),
        "generate definitions": Key("cs-d"),
        "expand": Key("c-npadd"),
        "collapse": Key("cs-minus") + Pause("2") + Key("cs-npmul") + Pause("2") + Key("1"),

        # Window handling.
        "next tab [<n>]": Key("a-right")*Repeat("n"),
        "previous tab [<n>]": Key("a-left")*Repeat("n"),
        "close tab": Key("c-f4"),
        "next split": Key("a-pgup"),
        "previous split": Key("a-pgdown"),

        # Version control.

        # Refactoring.
        "(refactor|re-factor) (this|choose)": Key("cas-t"),
        "(refactor|re-factor) rename": Key("s-f6"),
        "(refactor|re-factor) change signature": Key("c-f6"),
        "(refactor|re-factor) move": Key("f6"),
        "(refactor|re-factor) copy": Key("f5"),
        "(refactor|re-factor) safe delete": Key("a-del"),
        "(refactor|re-factor) extract variable": Key("ca-v"),
        "(refactor|re-factor) extract constant": Key("ca-c"),
        "(refactor|re-factor) extract field": Key("ca-f"),
        "(refactor|re-factor) extract parameter": Key("ca-p"),
        "(refactor|re-factor) extract method": Key("ca-m"),
        "(refactor|re-factor) (in line|inline)": Key("ca-n"),

    },
    [
        IntegerRef("line",1,10000),
    ],

)
Breathe.add_commands(
    ProxyCustomAppContext(title=".ipynb")
,
    {
        "run cell": Key("c-enter"),
        "run all": Key("cas-enter"),
        "insert cell above":Key("as-a"),
        "insert cell [below]":Key("as-b"),
        "insert mark down cell":Key("cas-0"),
        # "insert mark down cell":Key("cas-0"),
    },
    [

    ],
)
