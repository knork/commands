from voice2.imports import *
from dragonfly import Modifier, ShortIntegerRef

Breathe.add_global_extras(
    ShortIntegerRef("n", 1, 20, 1),
    Dictation("text", ""),
    Choice(
        "direction",
        {"lease": "left", "ross": "right", "sauce": "up", "dunce": "down"},
        "left",
    ),
)
