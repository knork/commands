from aenea import *
from breathe import Breathe, CommandContext
from voice2.utils import utilities
from voice2.utils.elements import Boolean, Choice
mod_char = "w"
CORE = utilities.load_toml_relative("config/core.toml")
Breathe.add_commands(
    None,
    {
        '(works|workspace|backspace) <n>': Key(mod_char + "-%(n)d"),
    # 'lock screen': Key(mod_char + "-d") + Text("i3lock") + Key("enter"),
    'go left [<n>]': Key(mod_char + "-left:%(n)d"),
    'go right [<n>]': Key(mod_char + "-right:%(n)d"),
    'go up [<n>]': Key(mod_char + "-up:%(n)d"),
    'go down [<n>]': Key(mod_char + "-down:%(n)d"),
    'move left [<n>]':  Key(mod_char + "s-left:%(n)d"),
    'move right [<n>]': Key(mod_char + "s-right:%(n)d"),
    'move up [<n>]': Key(mod_char + "s-up:%(n)d"),
    'move down [<n>]': Key(mod_char + "s-down:%(n)d"),
    'move <n>': Key(mod_char + "s-%(n)d"),
    'full-screen': Key(mod_char + "-f"),
    '(win|window) stacking': Key(mod_char + "-s"),
    '(win|window) default': Key(mod_char + "-e"),
    '(win|window) tabbed': Key(mod_char + "-w"),
    '(win|window) switch floating': Key(mod_char + "-space"),
    '(win|window) horizontal': Key(mod_char + "-h"),
    '(win|window) vertical': Key(mod_char + "-v"),
    '(win|window) terminal': Key(mod_char + "-enter"),
    '(win|window) vertical (term|terminal)': Key(mod_char + "-v, a-enter"),
    '(win|window) horizontal (term|terminal)': Key(mod_char + "-h, a-enter"),

    '(win|window) (kill|close)[<n>]':Key("ws-q/15:%(n)d") ,
    'window launch': Key(mod_char + "-d"),
    'window launch <program>': Key(mod_char + "-d") + Text("%(program)s")+ Key("enter"),
    'window launch  commander': Key(mod_char + "-enter")+Pause("75")+Text("mc")+Key("enter"),
    },
    [
        IntegerRef("line",1,10000),
        Choice("program", CORE["program"]),
    ],

)