from aenea import *
from breathe import Breathe, CommandContext,CommandsRef, Exec
from voice2.utils import utilities
from voice2.utils.actions import Read
from voice2.utils.elements import Boolean, Choice
from voice2.utils.execution import Alternating
from dragonfly import Modifier, ShortIntegerRef

CORE = utilities.load_toml_relative("config/core.toml")

release = Key("shift:up, ctrl:up, alt:up, win:up")
Breathe.add_commands(
    None,
    {
"[<big>] <letter>": lambda big, letter: Key(
            letter.upper() if big else letter
        ).execute(),
        "numbers <num_seq>": Text("%(num_seq)s"),
        'east [<n>]': Key('c-left') * Repeat("n"),
        'west [<n>]': Key('c-right') * Repeat("n"),
        "<key> [<n>]": Key("%(key)s") * Repeat("n"),

        "<keysingle>": Key("%(keysingle)s"),
        "<punctuation>": Key("%(punctuation)s"),
        "double <punctuation>": Key("%(punctuation)s") * Repeat(2),
        'bump [<n>]': Key('cs-right:%(n)d, del'),
        'pimp [<n>]': Key('cs-left:%(n)d, del'),
        "application": Key("apps"),

        "find <text>":Key("c-f")+Pause("3")+Text("%(text)s"),
# "find <text>":Key("c-f")+Pause("3")+Text("%(text)s"),
        "shift <dir> [<n>]": Key("shift:down") + Key("%(dir)s:%(n)d") + Key("shift:up"),
        "control <letter>": Key("control:down," + "%(letter)s," + "control:up"),
        "control <key>": Key("control:down," + "%(key)s," + "control:up"),
        "alt <letter>": Key("alt:down," + "%(letter)s," + "alt:up"),
        "super": Key("win:down,win:up"),
        "control hold": Key("ctrl:down"),
        'alt hold': Key('alt:down'),
        "alt <dir> [<n>]": Key("alt:down") + Key("%(dir)s:%(n)d") + Key("alt:up"),
        'release': release,

        "say <common>":Text("%(common)s"),
        "say <text>":Text("%(text)s"),
        "click [left]": Mouse("left"),
        "double click": Mouse("left") * Repeat(2),
        "click right": Mouse("right"),
        "click middle": Mouse("middle"),
        "set key map":Key("w-enter")+Pause("75")+Text("setxkbmap de")+Key("enter")+Pause("25")+Key("ws-q"),
    },
    extras=[
        Boolean("big"),
        Boolean("long"),
        Choice("common", CORE["commonwords"]),
        Choice("key", CORE["keys"]),
        Choice("keysingle", CORE["keyssingle"]),
        Choice("punctuation", CORE["punctuation"]),
Choice("letter", CORE["letters"]),
        Modifier(
            Repetition(IntegerRef("", 0, 10), min=1, max=5, name="num_seq"),
            lambda r: "".join(map(str, r)),
        ),

        Modifier(
            Repetition(Choice("direction", CORE["directional"]), min=1, max=4, name="dir"),
            lambda r: ", ".join(map(str, r)),
        ),
    ],
)

# def find_in(previous, character):
#     index=previous.find(character)
#     if index!=-1:
#         Key("home,right:"+str(index)).execute()
#
# Breathe.add_commands(
#     None,
#     {
#         "before <character>":Read("previous")+ Function(find_in,"%(character)s"),
#
#     },
#     [
#         Choice("character", CORE["letters"]),
#     ],
# )


from voice2.utils import textformat

Breathe.add_commands(
    None,
    {

        "[<sequence_of_commands>] print <printing>": Exec("sequence_of_commands")+Text("%(printing)s"),
        "[<sequence_of_commands>] saying <text> [brunt]": Exec("sequence_of_commands")+Function(
            textformat.master_format_text, capitalisation=0, spacing=0
        ),
        "[<sequence_of_commands>] (<capitalisation> <spacing> | <capitalisation> | <spacing>) <text> [brunt]": Exec("sequence_of_commands")+Function(textformat.master_format_text),
    },
    [
        Boolean("big"),
        Choice("capitalisation", CORE["capitalisation"], 0),
        Choice("spacing", CORE["spacing"], 0),
        Modifier(
            Repetition(Choice("letter", CORE["letters"]), min=1, max=12, name="printing"),
            lambda r: "".join(map(str, r)),
        ),
        CommandsRef("sequence_of_commands", 8),
    ],
    top_level=True
)
