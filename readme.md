# Installation
0. Setup Windows10 x64 VM (with virtualbox)
1. Buy and install Dragon Naturally Speaking on you're VM
2. Download [natlink](https://gitlab.com/knork/natlink2)
3. Extract repository and place under `C:\Natlink\Natlink\MacroSystem` you might need to rename some folders.
4. Copy the correct yd for you're python and dragon installation e.g. `natlink2_27_Ver15.pyd` from `MacroSystem\core\PYD` to `MacroSystem\core` and rename it to `natlink.pyd`
5. Open the terminal (cmd) as Admin and execute:
 `cd C:\Natlink\Natlink\MacroSystem\core`
 `regsvr32 natlink.pyd`
6. Open a text editor as admin
7. Open file `C:\ProgramData\Nuance\NaturallySpeaking15\ǹssystem.ini` and add `.Natlink=PythonSubsystem` to the [Global Clients].
After you're changes the file should look like this:
    ```
    [Global Clients]
    .Global=Default
    EditControl Support=Default
    IE Dictation Support=Default
    NLUT Support=Default
    DGN Compatibility Manager=Default
    .Natlink=PythonSub
    ```
8. Open the file `C:\ProgramData\Nuance\NaturallySpeaking15\ǹsapps.ini` and add:
    ``` 
    [.Natlink]
    App Support GUID={dd990001-bb89-11d2-b031-0060088dc929}
    ```  
    to the end of the file.
8.  Ensure that the natlink module can be found by:
    ```Windows Registry Editor Version 5.00
    
    [HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Python\PythonCore\2.7\PythonPath\NatLink]
    @="C:\\NatLink\\NatLink\\MacroSystem\\core" 
    ``` 
    for Python2.7.
9. In the terminal navigate to the folder that contains the pip-tool e.g. `cd C:\Python27\Scripts` and install
all dependencies:
`pip install dragonfly2 dfly-breathe six future pywin32 wxpython` 
10. (Optional) Test Natlink by copying `C:\Natlink\SampleMacros\_sample1.py` to `C:\Natlink\Natlink\MacroSystem` start dragon and say `demo sample one` into you're microphone. You should receive a confirmation the natlink message window.
11. Copy the client code from [aenea](https://github.com/dictation-toolbox/aenea) to `C:\Natlink\Natlink\MacroSystem`
12. On the **HOST** navigate to the server folder for your window manager e.g. ` aenea/server/linux_x11/`.
Rename  `aenea/server/linux_x11/config.py.example` to ` aenea/server/linux_x11/config.py` and start the server e.g. `python  aenea/server/linux_x11/server_x11.py`
13. In VirtualBox's networking settings, set the network to host-only adapter. This will make the default ip ` 192.168.56.1
` available.
14. Copy all contents of this repository to `C:\Natlink\Natlink\MacroSystem`. 
15. Restart Dragon and enjoy 
16. (Optional) Create a usb passthrough for reduced latency. To do so excectue `sudo adduser $USER vboxusers` on the host. 
After this you should be able to simply select your usb device from the user-interface.