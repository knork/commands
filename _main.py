import time
start = time.time()
import logging
import importlib

logging.basicConfig()
import sys, os
from voice2.imports import *

use_aenea=True

extras_module = {
    "voice2": {
        "core": ["globals"],
    }
}
modules = {
    "voice2": {
        "apps":["jetbrains","firefox","lyx","spotify","terminal"],
        "core": ["basics","i3"],
        "languages":["python"]
    }
}


Breathe.load_modules(extras_module)
Breathe.load_modules(modules)